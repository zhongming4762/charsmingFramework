package com.bootdo.framework.common.interceptor.mvc;

import com.bootdo.framework.common.util.EncryptUtil;
import org.springframework.http.HttpMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 接口加密拦截器，验证接口是否正常
 * @author zhongming
 * @since 3.0
 * 2018/5/9下午2:52
 */
public class EncryptInterceptor implements HandlerInterceptor {



    /**
     * 接口调用之前执行
     * @param request
     * @param response
     * @param handler
     * @return
     * @throws Exception
     */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        if(request.getMethod().equals(HttpMethod.OPTIONS.name())) {
            //option请求不拦截
            return true;
        }
        //验证签名
        return EncryptUtil.signatureSuccess(request);
//        if(EncryptUtil.signatureSuccess(request))
//        return true;
    }

    /**
     * 请求处理之后，接口调用之前执行
     * @param request
     * @param response
     * @param handler
     * @param modelAndView
     * @throws Exception
     */
    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        if(!request.getMethod().equals(HttpMethod.OPTIONS.name())) {
            System.out.println("请求之后===============");
        }
    }

    /**
     * 请求全部完成之后执行
     * @param request
     * @param response
     * @param handler
     * @param ex
     * @throws Exception
     */
    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        if(!request.getMethod().equals(HttpMethod.OPTIONS.name())) {
            System.out.println("试图渲染===============");
        }
    }
}
