package com.bootdo.framework.common.service;

import com.bootdo.framework.common.model.Log;
import com.bootdo.framework.common.model.Page;
import com.bootdo.framework.common.util.Query;
import org.springframework.stereotype.Service;

@Service
public interface LogService {
	void save(Log logDO);
	Page<Log> queryList(Query query);
	int remove(Long id);
	int batchRemove(Long[] ids);
}
