package com.bootdo.framework.common.service.impl;

import com.bootdo.framework.common.mapper.LogMapper;
import com.bootdo.framework.common.model.Log;
import com.bootdo.framework.common.model.Page;
import com.bootdo.framework.common.service.LogService;
import com.bootdo.framework.common.util.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LogServiceImpl implements LogService {
	@Autowired
	LogMapper logMapper;

	@Async
	@Override
	public void save(Log logDO) {
		 logMapper.save(logDO);
	}

	@Override
	public Page<Log> queryList(Query query) {
		int total = logMapper.count(query);
		List<Log> logs = logMapper.list(query);
		Page<Log> page = new Page<>();
		page.setTotal(total);
		page.setRows(logs);
		return page;
	}

	@Override
	public int remove(Long id) {
		int count = logMapper.remove(id);
		return count;
	}

	@Override
	public int batchRemove(Long[] ids){
		return logMapper.batchRemove(ids);
	}
}
