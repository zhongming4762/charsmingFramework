package com.bootdo.framework.common.service;


import com.bootdo.framework.common.model.Dict;
import com.bootdo.framework.system.model.User;

import java.util.List;
import java.util.Map;

/**
 * 字典表
 * 
 * @author chglee
 * @email 1992lcg@163.com
 * @date 2017-09-29 18:28:07
 */
public interface DictService {
	
	Dict get(Long id);
	
	List<Dict> list(Map<String, Object> map);
	
	int count(Map<String, Object> map);
	
	int save(Dict dict);
	
	int update(Dict dict);
	
	int remove(Long id);
	
	int batchRemove(Long[] ids);

	List<Dict> listType();
	
	String getName(String type, String value);

	/**
	 * 获取爱好列表
	 * @return
     * @param userDO
	 */
	List<Dict> getHobbyList(User userDO);

	/**
	 * 获取性别列表
 	 * @return
	 */
	List<Dict> getSexList();

	/**
	 * 根据type获取数据
	 * @return
	 */
	List<Dict> listByType(String type);

}
