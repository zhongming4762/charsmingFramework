package com.bootdo.framework.common.service.impl;


import com.bootdo.framework.common.config.LocalConfig;
import com.bootdo.framework.common.mapper.FileMapper;
import com.bootdo.framework.common.model.File;
import com.bootdo.framework.common.service.FileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.Map;


@Service
public class FileServiceImpl implements FileService {
	@Autowired
	private FileMapper sysFileMapper;

	@Autowired
	private LocalConfig localConfig;
	
	@Override
	public File get(Long id){
		return sysFileMapper.get(id);
	}
	
	@Override
	public List<File> list(Map<String, Object> map){
		return sysFileMapper.list(map);
	}
	
	@Override
	public int count(Map<String, Object> map){
		return sysFileMapper.count(map);
	}
	
	@Override
	public int save(File sysFile){
		return sysFileMapper.save(sysFile);
	}
	
	@Override
	public int update(File sysFile){
		return sysFileMapper.update(sysFile);
	}
	
	@Override
	public int remove(Long id){
		return sysFileMapper.remove(id);
	}
	
	@Override
	public int batchRemove(Long[] ids){
		return sysFileMapper.batchRemove(ids);
	}

    @Override
    public Boolean isExist(String url) {
		Boolean isExist = false;
		if (!StringUtils.isEmpty(url)) {
			String filePath = url.replace("/files/", "");
			filePath = localConfig.getUploadBasePath() + filePath;
			java.io.File file = new java.io.File(filePath);
			if (file.exists()) {
				isExist = true;
			}
		}
		return isExist;
	}
	}
