package com.bootdo.framework.common.filter.servlet;


import com.bootdo.framework.common.util.Constant;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 拦截器实现跨域访问
 * @author zhongming
 * @since 3.0
 * 2018/5/3下午2:30
 */
public class CORSFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

        HttpServletResponse res = (HttpServletResponse) servletResponse;
        HttpServletRequest request = (HttpServletRequest) servletRequest;


//		res.setContentType("textml;charset=UTF-8");
        res.setHeader("Access-Control-Allow-Origin", request.getHeader("Origin"));
        res.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE");
        res.setHeader("Access-Control-Max-Age", "0");

        res.setHeader("Access-Control-Allow-Headers", "authority, Origin, No-Cache, X-Requested-With, If-Modified-Since, Pragma, Last-Modified, " +
                "Cache-Control, Expires, Content-Type, X-E4M-With, userId, token, Content-Disposition, " + Constant.SIGNATURE_PARAMS + ", " + Constant.SIGNATURE);
        res.setHeader("Access-Control-Expose-Headers", "Content-Disposition");
        //允许跨域携带cookie
        res.setHeader("Access-Control-Allow-Credentials", "true");
        res.setHeader("XDomainRequestAllowed","1");

        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {

    }
}
