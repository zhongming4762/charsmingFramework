package com.bootdo.framework.common.config;

import org.springframework.beans.factory.config.YamlPropertiesFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.io.ClassPathResource;

/**
 * 读取配置文件
 * @author zhongming
 * @since 3.0
 * 2018/4/28下午2:21
 */
@Configuration
public class PropertySourcesRead {

    /**
     * 读取系统配置文件
     * @return
     */
    @Bean
    public static PropertySourcesPlaceholderConfigurer properties() {
        PropertySourcesPlaceholderConfigurer configurer = new PropertySourcesPlaceholderConfigurer();
        YamlPropertiesFactoryBean yaml = new YamlPropertiesFactoryBean();
        //读取shiro配置文件
        ClassPathResource shiroResource = new ClassPathResource("config/shiro.yml");
        yaml.setResources(shiroResource);
        configurer.setProperties(yaml.getObject());
        return configurer;
    }
}
