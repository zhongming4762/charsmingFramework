package com.bootdo.framework.common.config;

import com.bootdo.framework.common.filter.servlet.CORSFilter;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 拦截器配置类
 * @author zhongming
 * @since 3.0
 * 2018/5/3下午2:33
 */
@Configuration
public class FilterConfig {

    /**
     * 支持跨域的拦截器
     * @return
     */
    @Bean
    public FilterRegistrationBean corsFilter() {
        FilterRegistrationBean registration = new FilterRegistrationBean();
        registration.setFilter(new CORSFilter());
        registration.addUrlPatterns("/*");
        registration.setName("corsFilter");
        registration.setOrder(Integer.MAX_VALUE);
        return registration;
    }
}
