package com.bootdo.framework.common.config;

import com.bootdo.framework.common.util.CollectionUtil;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * 读取ShiroFilter配置的配置类
 * @author zhongming
 * @since 3.0
 * 2018/4/27下午4:47
 */
@Component
@ConfigurationProperties(prefix = "shiro.filterfactorybean")
public class ShiroFilterConfig {

    //登录url
    private String loginUrl;

    //登录成功url
    private String successUrl;

    //无权限页面
    private String unauthorizedUrl;

    private List<FilterChain> filterChainDefinitionList = new ArrayList<FilterChain>();

    private Map<String, String> filterChainDefinitionMap = new LinkedHashMap<>();


    @PostConstruct
    private void init() {
    }

    public String getLoginUrl() {
        return loginUrl;
    }

    public void setLoginUrl(String loginUrl) {
        this.loginUrl = loginUrl;
    }

    public String getSuccessUrl() {
        return successUrl;
    }

    public void setSuccessUrl(String successUrl) {
        this.successUrl = successUrl;
    }

    public String getUnauthorizedUrl() {
        return unauthorizedUrl;
    }

    public void setUnauthorizedUrl(String unauthorizedUrl) {
        this.unauthorizedUrl = unauthorizedUrl;
    }

    public Map<String, String> getFilterChainDefinitionMap() {
        if(!CollectionUtil.isEmpty(this.filterChainDefinitionList)) {
            for(FilterChain filterChain : this.filterChainDefinitionList) {
                this.filterChainDefinitionMap.put(filterChain.path, filterChain.value);
            }
        }
        return filterChainDefinitionMap;
    }

    public void setFilterChainDefinitionMap(Map<String, String> filterChainDefinitionMap) {
        this.filterChainDefinitionMap = filterChainDefinitionMap;
    }

    public List<FilterChain> getFilterChainDefinitionList() {
        return filterChainDefinitionList;
    }

    public void setFilterChainDefinitionList(List<FilterChain> filterChainDefinitionList) {
        this.filterChainDefinitionList = filterChainDefinitionList;
    }

    public static class FilterChain {
        private String path;
        private String value;

        public String getPath() {
            return path;
        }

        public void setPath(String path) {
            this.path = path;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }
    }
}
