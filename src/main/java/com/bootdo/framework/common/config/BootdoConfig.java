package com.bootdo.framework.common.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * "bootdo"配置信息
 * @author zhongming
 * @since 3.0
 * 2018/5/9下午3:34
 */
@Component
@ConfigurationProperties(prefix = "bootdo")
public class BootdoConfig {

    //本地化配置文件路径
    private String localConfig;

    //需要接口加密认证的url路径
    private List<String> encryptInterceptorUrls = new ArrayList<>();

    public String getLocalConfig() {
        return localConfig;
    }

    public void setLocalConfig(String localConfig) {
        this.localConfig = localConfig;
    }

    public List<String> getEncryptInterceptorUrls() {
        return encryptInterceptorUrls;
    }

    public void setEncryptInterceptorUrls(List<String> encryptInterceptorUrls) {
        this.encryptInterceptorUrls = encryptInterceptorUrls;
    }
}
