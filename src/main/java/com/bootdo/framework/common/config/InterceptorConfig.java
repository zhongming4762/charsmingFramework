package com.bootdo.framework.common.config;

import com.bootdo.framework.common.interceptor.mvc.EncryptInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.config.annotation.InterceptorRegistration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * mvc拦截器配置
 * @author zhongming
 * @since 3.0
 * 2018/5/9下午2:57
 */
@Configuration
public class InterceptorConfig implements WebMvcConfigurer {

    @Autowired
    private BootdoConfig bootdoConfig;

    /**
     * 接口加密过滤器
     * @return
     */
    @Bean
    public HandlerInterceptor encryptInterceptor() {
        return new EncryptInterceptor();
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        InterceptorRegistration addInterceptor = registry.addInterceptor(encryptInterceptor());
        addInterceptor.addPathPatterns(bootdoConfig.getEncryptInterceptorUrls());
    }
}
