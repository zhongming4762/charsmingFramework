package com.bootdo.framework.common.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 * 本地化配置信息
 * @author zhongming
 * @since 3.0
 * 2018/5/5上午11:23
 */
@Component
@PropertySource(value = {"classpath:${bootdo.localConfig}"}, encoding = "utf-8")
@ConfigurationProperties(prefix = "localconfig")
public class LocalConfig {

    /**
     * 文件上传路径，如果未指定，则使用项目路径
     */
    private String uploadBasePath;

    public String getUploadBasePath() {
        return uploadBasePath;
    }

    public void setUploadBasePath(String uploadBasePath) {
        this.uploadBasePath = uploadBasePath;
    }
}
