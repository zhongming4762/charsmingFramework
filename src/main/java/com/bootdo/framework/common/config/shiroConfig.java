package com.bootdo.framework.common.config;

import com.bootdo.framework.common.filter.shiro.OptionsAdoptFilter;
import com.bootdo.framework.common.security.shiro.*;
import com.bootdo.framework.common.service.RedisService;
import com.bootdo.framework.common.util.Constant;
import org.apache.shiro.cache.ehcache.EhCacheManager;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.session.SessionListener;
import org.apache.shiro.session.mgt.SessionManager;
import org.apache.shiro.session.mgt.eis.MemorySessionDAO;
import org.apache.shiro.session.mgt.eis.SessionDAO;
import org.apache.shiro.spring.LifecycleBeanPostProcessor;
import org.apache.shiro.spring.security.interceptor.AuthorizationAttributeSourceAdvisor;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.servlet.Filter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * shiro配置类
 * @author zhongming
 * @since 3.0
 * 2018/3/30下午5:14
 */
@Configuration
public class shiroConfig {

    @Value("${shiro.cacheType}")
    private String cacheType;

    @Value("${server.session-timeout}")
    private int sessionTimeout;


    @Bean
    public static LifecycleBeanPostProcessor getLifecycleBeanPostProcessor() {
        return new LifecycleBeanPostProcessor();
    }

    public Filter optionsAdoptFilter() {
        return new OptionsAdoptFilter();
    }

    @Bean
    public ShiroFilterFactoryBean shiroFilterFactoryBean(SecurityManager securityManager, ShiroFilterConfig shiroFilterConfig) {
        ShiroFilterFactoryBean shiroFilterFactoryBean = new ShiroFilterFactoryBean();
        shiroFilterFactoryBean.setSecurityManager(securityManager);
        shiroFilterFactoryBean.setLoginUrl(shiroFilterConfig.getLoginUrl());
        shiroFilterFactoryBean.setSuccessUrl(shiroFilterConfig.getSuccessUrl());
        shiroFilterFactoryBean.setUnauthorizedUrl(shiroFilterConfig.getUnauthorizedUrl());
        shiroFilterFactoryBean.setFilterChainDefinitionMap(shiroFilterConfig.getFilterChainDefinitionMap());
        shiroFilterFactoryBean.setFilters(getShiroFilters());
        return shiroFilterFactoryBean;
    }

    private Map<String, Filter> getShiroFilters() {
        Map<String, Filter> filters = new HashMap<String, Filter>();
        filters.put("optionsAdoptFilter", optionsAdoptFilter());
        return filters;
    }



    /**
     * 配置realm
     * @return 无状态realm
     */
    @Bean
    public StatelessRealm statelessRealm() {
        StatelessRealm statelessRealm = new StatelessRealm();
//        statelessRealm.set
        return statelessRealm;
    }

    /**
     * 自定义的sessionManager
     * @return
     */
    @Bean
    public SessionManager sessionManager(RedisService redisService) {
        MySessionManager statelessSessionManager = new MySessionManager();
        statelessSessionManager.setGlobalSessionTimeout(sessionTimeout * 1000);
        statelessSessionManager.setSessionDAO(sessionDAO(redisService));
        Collection<SessionListener> listeners = new ArrayList<SessionListener>();
        listeners.add(new BDSessionListener());
        statelessSessionManager.setSessionListeners(listeners);
        return statelessSessionManager;
    }

    /**
     * 配置securityManager
     * @return
     */
    @Bean
    public SecurityManager securityManager(@Autowired RedisService redisService) {
        DefaultWebSecurityManager securityManager = new DefaultWebSecurityManager();
        securityManager.setRealm(statelessRealm());
        // 自定义session管理 使用redis
        securityManager.setSessionManager(sessionManager(redisService));
        // 自定义缓存实现 使用redis
        if(Constant.CACHE_TYPE_REDIS.equals(this.cacheType)) {
            securityManager.setCacheManager(cacheManager(redisService));
        } else {
            securityManager.setCacheManager(ehCacheManager());
        }

        return securityManager;
    }

    /**
     * 生成RedisCacheManager
     * @return
     */
    public RedisCacheManager cacheManager(RedisService redisService) {
        RedisCacheManager cacheManager = new RedisCacheManager();
        cacheManager.setRedisService(redisService);
        return cacheManager;
    }


    /**
     * 配置EhCacheManager
     * @return
     */
    @Bean
    public EhCacheManager ehCacheManager() {
        EhCacheManager em = new EhCacheManager();
        em.setCacheManagerConfigFile("classpath:config/ehcache.xml");
        return em;
    }

    /**
     *  开启shiro aop注解支持.
     *  使用代理方式;所以需要开启代码支持;
     * @param securityManager
     * @return
     */
    @Bean
    public AuthorizationAttributeSourceAdvisor authorizationAttributeSourceAdvisor(SecurityManager securityManager){
        AuthorizationAttributeSourceAdvisor authorizationAttributeSourceAdvisor = new AuthorizationAttributeSourceAdvisor();
        authorizationAttributeSourceAdvisor.setSecurityManager(securityManager);
        return authorizationAttributeSourceAdvisor;
    }

    /**
     * 配置sessionDao
     * @return
     */
    @Bean
    public SessionDAO sessionDAO(RedisService redisService){
        if(Constant.CACHE_TYPE_REDIS.equals(cacheType)){
            return redisSessionDAO(redisService);
        }else {
            return new MemorySessionDAO();
        }
    }

    @Bean
    public RedisSessionDAO redisSessionDAO(RedisService redisService) {
        RedisSessionDAO redisSessionDAO = new RedisSessionDAO();
        redisSessionDAO.setRedisService(redisService);
        redisSessionDAO.setSessionTimeout(this.sessionTimeout);
        return redisSessionDAO;
    }


}
