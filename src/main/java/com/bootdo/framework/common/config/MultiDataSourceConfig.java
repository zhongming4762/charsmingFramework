package com.bootdo.framework.common.config;

import com.alibaba.druid.spring.boot.autoconfigure.DruidDataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import javax.sql.DataSource;

/**
 * 多数据源配置
 * @author zhongming
 * @since 3.0
 * 2018/3/29上午8:28
 */
@Configuration
public class MultiDataSourceConfig {

    /**
     * 系统库数据源
     * @return 数据源
     */
    @Primary
    @Bean(name = "sysDataSource")
    @ConfigurationProperties("spring.datasource.druid.sys")
    public DataSource dataSourceSys(){
        return DruidDataSourceBuilder.create().build();
    }

    /**
     * 业务库数据源
     * 主数据源
     * @return 数据源
     */
    @Bean(name = "serviceDataSource")
    @ConfigurationProperties("spring.datasource.druid.service")
    public DataSource dataSourceService(){
        return DruidDataSourceBuilder.create().build();
    }
}
