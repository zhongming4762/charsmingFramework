package com.bootdo.framework.common.security.shiro;

import com.bootdo.framework.common.service.RedisService;
import org.apache.shiro.cache.Cache;
import org.apache.shiro.cache.CacheException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 * 自定义的rediscache
 * @author zhongming
 * @since 3.0
 * 2018/4/27下午3:30
 */
public class RedisCache <K, V> implements Cache<K, V> {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    private RedisService redisService;

    //key的偏移量
    private String keyPrefix = "shiro_redis_session:";


    public RedisCache(RedisService redisService){
        if (redisService == null) {
            throw new IllegalArgumentException("Cache argument cannot be null.");
        }
        this.redisService = redisService;
    }

    public RedisCache(RedisService cache,
                      String prefix){

        this( cache );

        // set the prefix
        this.keyPrefix = prefix;
    }

    /**
     * 偏移key
     * @param key
     * @return
     */
    private String prefixKey(Object key) {
        return this.keyPrefix + key;
    }

    @Override
    public V get(K key) throws CacheException {
        logger.debug("根据key从Redis中获取对象 key [" + key + "]");
        try {
            if (key == null) {
                return null;
            }else{
                V value = (V) redisService.get(prefixKey(key));
                return value;
            }
        } catch (Throwable t) {
            throw new CacheException(t);
        }
    }

    @Override
    public V put(K k, V v) throws CacheException {
        logger.debug("根据key从存储 key [" + k + "]");
        try {
            redisService.put(prefixKey(k), v);
            return v;
        } catch (Throwable t) {
            throw new CacheException(t);
        }
    }

    @Override
    public V remove(K key) throws CacheException {
        logger.debug("从redis中删除 key [" + key + "]");
        try {
            V previous = get(key);
            this.redisService.delete(prefixKey(key));
            return previous;
        } catch (Throwable t) {
            throw new CacheException(t);
        }
    }

    /**
     * 删除所有缓存
     * @throws CacheException
     */
    @Override
    public void clear() throws CacheException {
        this.redisService.matchDelete(this.keyPrefix);
    }

    @Override
    public int size() {
        Set<Object> key = this.redisService.keys(this.keyPrefix);
        if(key != null) {
            return key.size();
        }
        return 0;
    }

    @Override
    public Set<K> keys() {
        Set<Object> keys = this.redisService.keys(this.keyPrefix);
        if(keys == null || keys.size() == 0) {
            return Collections.emptySet();
        } else {
            Set<K> keysNew = new HashSet<>(keys.size());
            for(Object key : keys) {
                keysNew.add((K) key);
            }
            return keysNew;
        }
    }

    @Override
    public Collection<V> values() {
        Set<Object> keys = redisService.keys(this.keyPrefix);
        List<Object> values = redisService.batchGet(keys);
        if(values == null || values.size() == 0) {
            return Collections.emptyList();
        } else {
            Collection<V> newValues = new ArrayList<V>();
            for(Object value : values) {
                if(value != null) {
                    newValues.add((V) value);
                }
            }
            return newValues;
        }
    }
}
