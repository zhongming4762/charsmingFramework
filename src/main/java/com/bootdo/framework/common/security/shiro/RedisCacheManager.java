package com.bootdo.framework.common.security.shiro;

import com.bootdo.framework.common.service.RedisService;
import org.apache.shiro.cache.Cache;
import org.apache.shiro.cache.CacheException;
import org.apache.shiro.cache.CacheManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * 自定义的CacheManager，使用redis实现
 * @author zhongming
 * @since 3.0
 * 2018/4/27下午4:07
 */
public class RedisCacheManager implements CacheManager {

    private static final Logger logger = LoggerFactory
            .getLogger(RedisCacheManager.class);

    /**
     * redis服务类
     */
    private RedisService redisService;
    private String keyPrefix = "shiro_redis_cache:";

    private final ConcurrentMap<String, Cache> caches = new ConcurrentHashMap<String, Cache>();

    @Override
    public <K, V> Cache<K, V> getCache(String name) throws CacheException {
        logger.debug("获取名称为: " + name + " 的RedisCache实例");
        Cache cache = caches.get(name);
        if(cache == null) {
            cache = new RedisCache(this.redisService, keyPrefix);
            caches.put(name, cache);
        }
        return cache;
    }

    public void setRedisService(RedisService redisService) {
        this.redisService = redisService;
    }

    public String getKeyPrefix() {
        return keyPrefix;
    }

    public void setKeyPrefix(String keyPrefix) {
        this.keyPrefix = keyPrefix;
    }
}
