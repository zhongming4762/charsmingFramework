package com.bootdo.framework.common.security.shiro;

import com.bootdo.framework.common.service.RedisService;
import org.apache.shiro.session.Session;
import org.apache.shiro.session.UnknownSessionException;
import org.apache.shiro.session.mgt.eis.AbstractSessionDAO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author zhongming
 * @since 3.0
 * 2018/4/27下午5:28
 */
public class RedisSessionDAO extends AbstractSessionDAO {

    private static Logger logger = LoggerFactory.getLogger(RedisSessionDAO.class);

    private RedisService redisService;

    private String keyPrefix = "shiro_redis_session:";

    private int sessionTimeout;

    @Override
    protected Serializable doCreate(Session session) {
        Serializable sessionId = this.generateSessionId(session);
        this.assignSessionId(session, sessionId);
        this.saveSession(session);
        return sessionId;
    }



    @Override
    protected Session doReadSession(Serializable sessionId) {
        if(sessionId == null){
            logger.error("session id is null");
            return null;
        }

        Session session = (Session) redisService.get(this.prefixKey(sessionId));
//        Session s = (Session)SerializeUtils.deserialize(redisManager.get(this.getByteKey(sessionId)));
        return session;
    }

    @Override
    public void update(Session session) throws UnknownSessionException {
        this.saveSession(session);
    }

    @Override
    public void delete(Session session) {
        if(session == null || session.getId() == null){
            logger.error("session or session id is null");
            return;
        }
        redisService.delete(this.prefixKey(session.getId()));
//        redisManager.del(this.getByteKey(session.getId()));
    }

    @Override
    public Collection<Session> getActiveSessions() {
        Set<Session> sessions = new HashSet<Session>();

        Set<Object> keys = this.redisService.keys(this.keyPrefix + "*");
        if(keys != null && keys.size() > 0) {
            List<Object> objects = this.redisService.batchGet(keys);
            for(Object object : objects) {
                Session session = (Session) object;
                sessions.add(session);
            }
        }

//        Set<byte[]> keys = redisManager.keys(this.keyPrefix + "*");
//        if(keys != null && keys.size()>0){
//            for(byte[] key:keys){
//                Session s = (Session)SerializeUtils.deserialize(redisManager.get(key));
//                sessions.add(s);
//            }
//        }

        return sessions;
    }

    private void saveSession(Session session) {
        if(session == null || session.getId() == null){
            logger.error("session or session id is null");
            return;
        }

        session.setTimeout(this.sessionTimeout * 1000);
        this.redisService.put(this.prefixKey(session.getId()).toString(), session, this.sessionTimeout);
//        this.redisManager.set(session.getId(), session, redisManager.getExpire());
    }

    public void setRedisService(RedisService redisService) {
        this.redisService = redisService;
    }

    private String prefixKey(Object key) {
        return this.keyPrefix + key.toString();
    }

    public int getSessionTimeout() {
        return sessionTimeout;
    }

    public void setSessionTimeout(int sessionTimeout) {
        this.sessionTimeout = sessionTimeout;
    }
}
