package com.bootdo.framework.common.controller;

import com.bootdo.framework.common.util.ShiroUtils;
import com.bootdo.framework.system.model.User;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.WebDataBinder;

import java.beans.PropertyEditorSupport;
import java.text.SimpleDateFormat;
import java.util.Date;

@Controller
public class BaseController {
	public User getUser() {
		return ShiroUtils.getUser();
	}

	public Long getUserId() {
		return getUser().getUserId();
	}

	public String getUsername() {
		return getUser().getUsername();
	}


	/**
	 * 处理日期类型上传为null的问题
	 * springMVC表单未填写传到后台会变成空串，而不是null，如果是非String类型，转换会出错
	 * @param binder
	 */
	@org.springframework.web.bind.annotation.InitBinder
	protected void InitBinderDate(WebDataBinder binder) {

		//springMVC表单未填写传到后台会变成空串，而不是null，如果是非String类型，转换会出错
		binder.registerCustomEditor(Date.class, new PropertyEditorSupport() {



			@Override
			public void setAsText(String value) throws IllegalArgumentException {
				SimpleDateFormat dateFormat = getSimpleDateFormat(value);
				try {
					setValue(dateFormat.parse(value));
				} catch (Exception e) {
					e.printStackTrace();
					setValue(null);
				}
			}

			@Override
			public String getAsText() {
				return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format((Date) getValue());
			}
		});
	}


	/**
	 * 处理字符串问题，前台未填写传到后台会变成空串，此方法将其转成null
	 * @param binder
	 */
	@org.springframework.web.bind.annotation.InitBinder
	protected void InitBinderString(WebDataBinder binder) {
		// 处理字符串问题，前台未填写传到后台会变成空串，此方法将其转成null
		binder.registerCustomEditor(String.class, new PropertyEditorSupport() {

			@Override
			public void setAsText(String text) throws IllegalArgumentException {
				if (StringUtils.isEmpty(text)) {
					setValue(null);
				} else {
					setValue(text);
				}
			}

		});
	}

	/**
	 * 根据字符串类型返回SimpleDateFormat
	 * @param str
	 * @return
	 */
	private SimpleDateFormat getSimpleDateFormat(String str) {
		//匹配yyyy-MM-dd
		String match1 = "^((?:19|20)\\d\\d)-(0[1-9]|1[012])-(0[1-9]|[12][0-9]|3[01])$";
		//匹配yyyy-MM-dd HH:mm:ss
		String mathch2 = "^((\\d{2}(([02468][048])|([13579][26]))[\\-\\/\\s]?((((0?[13578])|(1[02]))[\\-\\/\\s]?((0?[1-9])|([1-2][0-9])|(3[01])))|(((0?[469])|(11))[\\-\\/\\s]?((0?[1-9])|([1-2][0-9])|(30)))|(0?2[\\-\\/\\s]?((0?[1-9])|([1-2][0-9])))))|(\\d{2}(([02468][1235679])|([13579][01345789]))[\\-\\/\\s]?((((0?[13578])|(1[02]))[\\-\\/\\s]?((0?[1-9])|([1-2][0-9])|(3[01])))|(((0?[469])|(11))[\\-\\/\\s]?((0?[1-9])|([1-2][0-9])|(30)))|(0?2[\\-\\/\\s]?((0?[1-9])|(1[0-9])|(2[0-8]))))))(\\s((([0-1][0-9])|(2?[0-3]))\\:([0-5]?[0-9])((\\s)|(\\:([0-5]?[0-9])))))?$";
		//匹配yyyy-MM-dd HH:mm
		String match3 = "\\d{4}-(([0][1,3-9]|[1][0,1,2])-([0,1,2]\\d|[3][0,1])|(02-[0,1,2]\\d))\\s+([0,1]\\d|[2][0,1,2,3]):([0,1,2,3,4,5]\\d)";
		SimpleDateFormat dateFormat = null;
		if(str.matches(match1)) {
			dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		} else if(str.matches(mathch2)) {
			dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		} else if(str.matches(match3)) {
			dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		}
		return dateFormat;
	}
}