package com.bootdo.framework.common.mapper;

import com.bootdo.framework.common.model.Log;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * @author zhongming
 * @since 3.0
 * 2018/5/7下午5:41
 */
@Mapper
public interface LogMapper {

    Log get(Long id);

    List<Log> list(Map<String, Object> map);

    int count(Map<String, Object> map);

    int save(Log log);

    int update(Log log);

    int remove(Long id);

    int batchRemove(Long[] ids);
}
