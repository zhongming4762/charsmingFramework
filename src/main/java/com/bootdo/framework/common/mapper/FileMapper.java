package com.bootdo.framework.common.mapper;

import com.bootdo.framework.common.model.File;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * @author zhongming
 * @since 3.0
 * 2018/5/5上午11:38
 */
@Mapper
public interface FileMapper {

    File get(Long id);

    List<File> list(Map<String, Object> map);

    int count(Map<String, Object> map);

    int save(File file);

    int update(File file);

    int remove(Long id);

    int batchRemove(Long[] ids);
}
