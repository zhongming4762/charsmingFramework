package com.bootdo.framework.common.mapper;

import com.bootdo.framework.common.model.Dict;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * @author zhongming
 * @since 3.0
 * 2018/5/15下午5:17
 */
@Mapper
public interface DictMapper {

    Dict get(Long id);

    List<Dict> list(Map<String, Object> map);

    int count(Map<String, Object> map);

    int save(Dict dict);

    int update(Dict dict);

    int remove(Long id);

    int batchRemove(Long[] ids);

    List<Dict> listType();
}
