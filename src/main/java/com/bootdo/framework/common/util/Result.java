package com.bootdo.framework.common.util;

import java.lang.reflect.Field;

/**
 * 结果封装
 * @author zhongming
 * @since 3.0
 * 2018/5/4下午3:11
 */
public class Result {

    private String code;

    private String message;

    private Object data;

    private Boolean ok = true;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    private static Result newInstance() {
        return new Result();
    }

    public static Result success(Object data) {
        Result result = newInstance();
        result.setData(data);
        result.setCode(ResultCodeEnum.SUCCESS.getCode());
        result.setMessage(ResultCodeEnum.SUCCESS.getMsg());

        if (data == null) {
            result.setData("");
        } else {
            Field[] fields = data.getClass().getDeclaredFields();// 遇到没有属性的空类,防止JSON转换的时候异常
            if (fields.length == 0) {
                result.setData("");
            }
        }
        return result;
    }

    public static Result failure(String message) {
        Result result = newInstance();
        result.setCode(ResultCodeEnum.FAILURE.getCode());
        result.setMessage(message);
        result.ok = false;
        return result;
    }

    public static Result Result(String errorCode, String message) {
        Result result = newInstance();
        result.setCode(errorCode);
        result.setMessage(message);
        return result;
    }

    public static Result failure(String errorCode, String message, Object data) {
        Result result = newInstance();
        result.setCode(errorCode);
        result.setMessage(message);
        result.setData(data);

        if (data == null) {
            result.setData("");
        }
        result.ok = false;
        return result;
    }

    /**
     * 获取失败对象
     * @author ming
     * @param message 返回信息
     * @param data 返回数据
     * @return 失败对象
     */
    public static Result failure(String message, Object data) {
        return failure(ResultCodeEnum.FAILURE.getCode(), message, data);
    }
}
