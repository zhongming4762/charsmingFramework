package com.bootdo.framework.common.util;

import java.util.Collection;

/**
 * @author zhongming
 * @since 3.0
 * 2018/4/28下午3:39
 */
public class CollectionUtil {

    /**
     * 判断Collection是否为null
     * @param collection
     * @return
     */
    public static boolean isEmpty(Collection collection) {
        if(collection != null && collection.size() >0) {
            return false;
        }
        return true;
    }
}
