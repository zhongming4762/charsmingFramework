package com.bootdo.framework.common.util;

import org.apache.shiro.crypto.hash.SimpleHash;

/**
 * MD5加密工具类
 * @author zhongming
 * @since 3.0
 * 2018/5/9下午4:44
 */
public class SecurityUtil {

    /**
     * MD5加密
     */
    public static final String MD5 = "md5";

    /**
     * MD5加密
     * @param value 需要加密的值
     * @param salt 偏移
     * @param hashIterations 迭代次数
     * @return 加密的字符串
     */
    public static String MD5Encrypt(Object value, String salt, int hashIterations) {
        return new SimpleHash(MD5, value, salt, hashIterations).toHex();
    }

    public static void main(String[] args) {
        System.out.println("======" + MD5Encrypt("abc", null, 2));
    }
}
