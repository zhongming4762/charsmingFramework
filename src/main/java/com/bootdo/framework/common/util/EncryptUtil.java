package com.bootdo.framework.common.util;

import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Map;

/**
 * 加密工具类
 * @author zhongming
 * @since 3.0
 * 2018/5/9下午4:00
 */
public class EncryptUtil {

    //迭代次数
    private static final Integer HASH_ITERATIONS = 1;


    /**
     * 生成接口加密token
     * @return
     */
    public static String createEncryptToken() {
        return SecurityUtil.MD5Encrypt(UUIDGenerator.getUUID(), null, 1);
    }

    /**
     * 生成签名信息
     * 通过token，随机串，时间戳、Encrypt_token生成签名
     * @param request
     * @return
     */
    public static String createSignature(HttpServletRequest request) throws IOException {
        String encryptMessageStr = request.getHeader(Constant.SIGNATURE_PARAMS);
        if(StringUtils.isEmpty(encryptMessageStr)) {
            return "";
        }
        Map<String, String> encryptMessage = JSONUtils.jsonToMap(encryptMessageStr);
        //获取随机串
        String noncestr = encryptMessage.get("noncestr");
        //获取时间戳
        String timestamp = encryptMessage.get("timestamp");
        //从session中获取
        String encryptToken = (String) request.getSession().getAttribute(Constant.ENCRYPT_TOKEN);

        String string = "token=" + ShiroUtils.getToken() + "&noncestr" + noncestr + "&timestamp" + timestamp + "&encryptToken" + encryptToken;

//        return SecurityUtil.MD5Encrypt(string, ShiroUtils.getUser().getUsername(), HASH_ITERATIONS);
        //TODO 待完善 前端盐值迭代加密未解决
        return SecurityUtil.MD5Encrypt(string, null, HASH_ITERATIONS);
    }

    /**
     * 验证签名是否成功
     * @param request 请求体
     * @return
     * @throws IOException
     */
    public static boolean signatureSuccess(HttpServletRequest request) throws IOException {
        //生成签名
        String signature = createSignature(request);
        //从前台请求中获取签名
        String signatureFromFront = request.getHeader(Constant.SIGNATURE);
        if(signature.equals(signatureFromFront)) {
            return true;
        }
        return false;
    }
}
