package com.bootdo.framework.common.util;

public class Constant {
    //演示系统账户
    public static String DEMO_ACCOUNT = "test";
    //自动去除表前缀
    public static String AUTO_REOMVE_PRE = "true";
    //停止计划任务
    public static String STATUS_RUNNING_STOP = "stop";
    //开启计划任务
    public static String STATUS_RUNNING_START = "start";
    //通知公告阅读状态-未读
    public static String OA_NOTIFY_READ_NO = "0";
    //通知公告阅读状态-已读
    public static int OA_NOTIFY_READ_YES = 1;
    //部门根节点id
    public static Long DEPT_ROOT_ID = 0l;
    //缓存方式
    public static String CACHE_TYPE_REDIS ="redis";

    public static String LOG_ERROR = "error";

    //session存储encryptToken的key
    public static final String ENCRYPT_TOKEN = "encryptToken";

    //存储在请求头中的签名参数key
    public static final String SIGNATURE_PARAMS = "signatureParams";

    //存储在请求头中签名key
    public static final String SIGNATURE = "signature";

    public static final String AUTHORITY = "authority";

    
}
