package com.bootdo.framework.system.controller;

import com.bootdo.framework.common.annotation.Log;
import com.bootdo.framework.common.controller.BaseController;
import com.bootdo.framework.common.model.File;
import com.bootdo.framework.common.model.Tree;
import com.bootdo.framework.common.service.FileService;
import com.bootdo.framework.common.util.*;
import com.bootdo.framework.system.model.Menu;
import com.bootdo.framework.system.service.MenuService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class LoginController extends BaseController {
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	MenuService menuService;
	@Autowired
	FileService fileService;
	@GetMapping({ "/", "" })
	String welcome(Model model) {

		return "redirect:/blog";
	}

	@Log("请求访问主页")
	@GetMapping({ "/index" })
	String index(Model model) {
		List<Tree<Menu>> menus = menuService.listMenuTree(getUserId());
		model.addAttribute("menus", menus);
		model.addAttribute("name", getUser().getName());
		File fileDO = fileService.get(getUser().getPicId());
		if(fileDO!=null&&fileDO.getUrl()!=null){
			if(fileService.isExist(fileDO.getUrl())){
				model.addAttribute("picUrl",fileDO.getUrl());
			}else {
				model.addAttribute("picUrl","/img/photo_s.jpg");
			}
		}else {
			model.addAttribute("picUrl","/img/photo_s.jpg");
		}
		model.addAttribute("username", getUser().getUsername());
		return "index_v1";
	}

	@GetMapping("/login")
	String login() {
		return "login";
	}

	@Log("登录")
	@PostMapping("/login")
	@ResponseBody
	R ajaxLogin(String username, String password) {

		return this.doLogin(username, password);
	}

	@Log("移动端登录")
	@PostMapping("/mobileLogin")
	@ResponseBody
	public Result mobileLogin(@RequestBody Map<String, String> parameterSet) {
		String username = parameterSet.get("username");
		String password = parameterSet.get("password");
		R result = doLogin(username, password);
		if(result.getSuccess()) {
			Map<String, String> data = new HashMap<>();
			data.put(Constant.AUTHORITY, SecurityUtils.getSubject().getSession().getId().toString());
			String encryptToken = EncryptUtil.createEncryptToken();
			data.put(Constant.ENCRYPT_TOKEN, encryptToken);
			data.put("username", ShiroUtils.getUser().getUsername());
			SecurityUtils.getSubject().getSession().setAttribute(Constant.ENCRYPT_TOKEN, encryptToken);
			return Result.success(data);
		} else {
			return Result.failure(result.get("msg").toString());
		}
	}

	/**
	 * 具体执行登录
	 * @param username
	 * @param password
	 * @return
	 */
	private R doLogin(String username, String password) {
		if(org.springframework.util.StringUtils.isEmpty(username) || org.springframework.util.StringUtils.isEmpty(password)) {
			return R.error("用户或密码不能为空");
		}
		password = MD5Utils.encrypt(username, password);
		UsernamePasswordToken token = new UsernamePasswordToken(username, password);
		Subject subject = SecurityUtils.getSubject();
		try {
			subject.login(token);
			return R.ok();
		} catch (AuthenticationException e) {
			return R.error("用户或密码错误");
		}
	}


	@GetMapping("/logout")
	String logout() {
		ShiroUtils.logout();
		return "redirect:/login";
	}

	@GetMapping("/main")
	String main() {
		return "main";
	}

}
