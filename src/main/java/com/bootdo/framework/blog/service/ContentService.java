package com.bootdo.framework.blog.service;


import com.bootdo.framework.blog.model.Content;

import java.util.List;
import java.util.Map;

/**
 * 文章内容
 * 
 * @author chglee
 * @email 1992lcg@163.com
 * @date 2017-09-09 10:03:34
 */
public interface ContentService {
	
	Content get(Long cid);
	
	List<Content> list(Map<String, Object> map);
	
	int count(Map<String, Object> map);
	
	int save(Content bContent);
	
	int update(Content bContent);
	
	int remove(Long cid);
	
	int batchRemove(Long[] cids);
}
