package com.bootdo.framework.blog.service.impl;

import com.bootdo.framework.blog.mapper.ContentMapper;
import com.bootdo.framework.blog.model.Content;
import com.bootdo.framework.blog.service.ContentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


@Service
public class ContentServiceImpl implements ContentService {
	@Autowired
	private ContentMapper bContentMapper;
	
	@Override
	public Content get(Long cid){
		return bContentMapper.get(cid);
	}
	
	@Override
	public List<Content> list(Map<String, Object> map){
		return bContentMapper.list(map);
	}
	
	@Override
	public int count(Map<String, Object> map){
		return bContentMapper.count(map);
	}
	
	@Override
	public int save(Content bContent){
		return bContentMapper.save(bContent);
	}
	
	@Override
	public int update(Content bContent){
		return bContentMapper.update(bContent);
	}
	
	@Override
	public int remove(Long cid){
		return bContentMapper.remove(cid);
	}
	
	@Override
	public int batchRemove(Long[] cids){
		return bContentMapper.batchRemove(cids);
	}
	
}
