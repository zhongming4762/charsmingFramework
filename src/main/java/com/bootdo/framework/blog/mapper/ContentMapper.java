package com.bootdo.framework.blog.mapper;

import com.bootdo.framework.blog.model.Content;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 文章内容
 * @author chglee
 * @email 1992lcg@163.com
 * @date 2017-10-03 16:17:48
 */
@Mapper
public interface ContentMapper {

	Content get(Long cid);
	
	List<Content> list(Map<String, Object> map);

	int count(Map<String, Object> map);
	
	int save(Content content);
	
	int update(Content content);
	
	int remove(Long cid);
	
	int batchRemove(Long[] cids);
}
